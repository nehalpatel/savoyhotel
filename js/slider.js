//find all html elements
var container = document.getElementById('gallery');

//

container.id = 'slider';


//find all the figure and spred them out

var allFigures = container.getElementsByTagName('figure');

// loop thruogh each figure (line up)
for (var i=0; i<allFigures.length; i++){
	allFigures[i].style.left = i * 100 + '%';
}

// javascript interval
//run code over Time
setTimeout(function(){
	sliderTimer = setInterval(animate, 20);
}, 3000 );

//var sliderTimer = setInterval(animate, 20);


function animate(){

//loop through each figure and move slightly
for (var i=0; i<allFigures.length; i++) {
	
	//find out what left figure % is
	var correntPercent = allFigures[i].style.left;

	//remove percent symbol
	correntPercent = parseInt(correntPercent);

	//reduce the percent by little bit
	correntPercent -= 1; 

	//apply new percentage to the figure
	allFigures[i].style.left = correntPercent + '%';

	//stoping figure from going off the screen
	if( correntPercent <= -100){
		
		//move this figure to the back of the line
		var backPosition = allFigures.length * 100 - 100 + '%';

		allFigures[i].style.left = backPosition;


		// stop the interval and wait a little bit
		clearInterval(sliderTimer);

		//wait
		setTimeout(restartTimer, 3000);

	}


	//console.log(correntPercent);
}

}

function restartTimer(){
	sliderTimer = setInterval(animate, 20);
}




